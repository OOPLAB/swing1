/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author User
 */
class MyActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener");
    }

}

public class HolloMe implements ActionListener {

    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblYouName = new JLabel("You Name :"); // ข้อความ
        lblYouName.setSize(80, 20);
        lblYouName.setLocation(5, 5);// การจัดตำเเหน่งบนFrame
        lblYouName.setBackground(Color.WHITE);//ใส่สี
        lblYouName.setOpaque(true);//ความทึบ

        JTextField txtYourName = new JTextField(); // ช่อง
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90, 5);// การจัดตำเเหน่งบนFrame
        txtYourName.setBackground(Color.WHITE);//ใส่สี
        txtYourName.setOpaque(true);//ความทึบ

        JButton btnHello = new JButton("Hello");// ปุ่ม
        btnHello.setSize(80, 20);
        btnHello.setLocation(90, 40);// การจัดตำเเหน่งบนFrame
        btnHello.setBackground(Color.WHITE);//ใส่สี
        btnHello.setOpaque(true);//ความทึบ

        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener); // เมื่อกดปุ่มจะเรียกให้ myActionListener ทำงาน
        btnHello.addActionListener(new HolloMe());// เมื่อกดปุ่มจะเรียกให้ HolloMe()ทำงาน

        ActionListener actionListener = new ActionListener() { // Anonymous Class 
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class: Action");
            }

        };
        btnHello.addActionListener(actionListener);

        JLabel lblHello = new JLabel("Hello ...", JLabel.CENTER); // ข้อความ
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);// การจัดตำเเหน่งบนFrame
        lblHello.setBackground(Color.WHITE);//ใส่สี
        lblHello.setOpaque(true);//ความทึบ

        frmMain.setLayout(null);// จัด Layout
        frmMain.add(lblYouName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lblHello);
        
         btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name =txtYourName.getText();
                lblHello.setText("Hello "+name);
                
            }
        });
        
        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Hello Me Action");
    }
}
