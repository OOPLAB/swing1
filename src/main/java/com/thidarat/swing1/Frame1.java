/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.swing1;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author User
 */
public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame(); //Create frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//EXIT_ON_CLOSE ปิดFrame
        frame.setSize(500, 300); // Size frame
        
        
        JLabel lblHelloWorld = new JLabel("Hello World",JLabel.CENTER); // JLabel ข้อความที่ทำอะไรไม่ได้ JLabel.CENTER ทำให้ข้อความอยู่ตรงกลาง
        lblHelloWorld.setBackground(Color.green);//ใส่สีพื้นหลัง
        lblHelloWorld.setOpaque(true);//Opaque ความทึบของสี
        lblHelloWorld.setFont(new Font("Verdana", Font.PLAIN, 30));//ใส่Font เเละขนาด Font ของข้อความ
        frame.add(lblHelloWorld);// ใส่ข้อความในframe
        
        frame.setVisible(true);// setVisible  ทำให้มองเห็น set กำหนดค่า get เรียกค่า
           
    }
}
